# Changelog

### 1.0.0 (2021-04-14)

* initial public release

### 1.0.1 (2021-04-16)

* **locations:** fix: use correct base32 decoding
* **scanner:** fix: add v3b signature format support
* **backend:** fix: fix redis initialization
* **backend:** feat: add rate limit by phone number